"""

app.py

author:     j.tatman

objectives: pull stuff from alphavantage.co
            stay within decent rate limits 
            roll outbound using zeromq publish
            create a simple subscriber api etc.
            display stuff usefully

requires:   alphavantage api key : https://www.alphavantage.co/support/#api-key

"""

"""

examples for api:

    Bitcoin to Chinese Yuan:
        https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=CNY&apikey=demo

    US Dollar to Japanese Yen:
        https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=JPY&apikey=demo

"""


from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"









